package com.example.ala.eventaggregator;

/**
 * Created by ala on 14/07/16.
 */
public enum Event {

    NEW_IMAGE,
    PROCESSED_IMAGE,
    VISIBLE_CASCADES,
    NEW_FACE_DETECTED,
    TAIL_TO_DRAW, NEW_PULSE,

}
