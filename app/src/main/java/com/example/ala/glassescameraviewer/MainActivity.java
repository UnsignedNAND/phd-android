package com.example.ala.glassescameraviewer;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.ala.config.ColorSpace;
import com.example.ala.config.Config;
import com.example.ala.config.ConfigActivity;
import com.example.ala.eventaggregator.Event;
import com.example.ala.eventaggregator.EventAggregator;
import com.example.ala.eventaggregator.EventListener;
import com.example.ala.regiondetector.RegionDetector;
import com.example.ala.utils.ColorUtil;
import com.example.ala.utils.FrameUtil;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements EventListener, CameraBridgeViewBase.CvCameraViewListener{

    private FaceView mFaceView;
    private GraphView graph;
    private TextView textView;
    private TextView textPulse;
    private CameraBridgeViewBase mOpenCvCameraView;
    private RegionDetector regionDetector;
    private Button buttonRGB;
    private Button buttonYUV;
    private Button buttonch1;
    private Button buttonch2;
    private Button buttonch3;
    private Button buttonConfig;

    private EventAggregator eventAggregator;
    private CascadeClassifier faceCascadeClassifier;
    private CascadeClassifier eyeCascadeClassifier;

    private double heightProportion = 0.6;
    private double widthProportion = 0.3;
    private String tooFarMsg = "Move closer";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        mFaceView = (FaceView) findViewById( R.id.face_overlay );
        textView = (TextView) findViewById(R.id.textView);
        textView.setText(tooFarMsg);
        textPulse = (TextView) findViewById(R.id.textPulse);
        textPulse.setText("not known");

        buttonRGB = (Button) findViewById(R.id.button);
        buttonRGB.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Config.SetColorSpace(ColorSpace.RGB);
            }
        });

        buttonYUV = (Button) findViewById(R.id.button2);
        buttonYUV.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Config.SetColorSpace(ColorSpace.YUV);
            }
        });

        buttonch1 = (Button) findViewById(R.id.button3);
        buttonch1.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Config.SetChannel(1);
            }
        });
        buttonch2 = (Button) findViewById(R.id.button4);
        buttonch2.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Config.SetChannel(2);
            }
        });
        buttonch3 = (Button) findViewById(R.id.button5);
        buttonch3.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Config.SetChannel(3);
            }
        });

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        eventAggregator = new EventAggregator();
        eventAggregator.addEventListener(Event.PROCESSED_IMAGE, this);
        eventAggregator.addEventListener(Event.NEW_FACE_DETECTED, this);
        eventAggregator.addEventListener(Event.TAIL_TO_DRAW, this);
        eventAggregator.addEventListener(Event.NEW_PULSE, this);

        regionDetector = new RegionDetector(eventAggregator);


        graph = (GraphView) findViewById(R.id.graphView1);

        buttonConfig = (Button) findViewById(R.id.config);

    }

    public BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    loadCascades();
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setAlpha(0);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    private void loadCascades() {
        faceCascadeClassifier = initializeOpenCVDependencies(R.raw.lbpcascade_frontalface, "lbpcascade_frontalface.xml");
        eyeCascadeClassifier = initializeOpenCVDependencies(R.raw.haarcascade_mcs_eyepair_big, "haarcascade_mcs_eyepair_big.xml");
        eventAggregator.triggerEvent(Event.VISIBLE_CASCADES, faceCascadeClassifier, eyeCascadeClassifier);
    }

    private CascadeClassifier initializeOpenCVDependencies(int i, String name) {
        CascadeClassifier cascadeClassifier = null;
        try {
            // Copy the resource into a temp file so OpenCV can load it
            InputStream is = getResources().openRawResource(i);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, name);
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            // Load the cascade classifier
            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            cascadeClassifier.load(mCascadeFile.getAbsolutePath());
        } catch (Exception e) {
            Log.e("OpenCVActivity", "Error loading cascade", e);
        }
        return cascadeClassifier;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }

    @Override
    public void onEventOccurred(Event event, final Object parameter, Object parameter2) {
       if(event==Event.PROCESSED_IMAGE){
           final Bitmap bitmap = FrameUtil.matToBitmap((Mat)parameter);
           runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   mFaceView.setBitmap(bitmap);
                   mFaceView.invalidate();
               }
           });
        }
        else if(event==Event.NEW_FACE_DETECTED){
           setDistanceMsg((Rect) parameter);
       }
        else if (event==Event.TAIL_TO_DRAW){
           drawGraph((List<Double>)parameter);
       }
        else if(event==Event.NEW_PULSE){
           setPulseMsg((Double)parameter);
       }

    }

    private void drawGraph(final List<Double> listOfColorsValues) {
        final LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[] {});
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(int i=0; i<listOfColorsValues.size(); i++){
                    DataPoint dp = new DataPoint(i, listOfColorsValues.get(i));
                    series.appendData(dp, true, listOfColorsValues.size());

                }
                graph.addSeries(series);
            }
        });
    }

    private void setDistanceMsg(Rect parameter) {
        final Rect faceRect = parameter;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(faceRect.size().height/(double)mFaceView.getHeight() < heightProportion &&
                        faceRect.size().width/(double)mFaceView.getWidth() < widthProportion) {
                    textView.setText(tooFarMsg);
                    mFaceView.setColor(Color.RED);
                }
                else {
                    textView.setText("");
                    mFaceView.setColor(Color.BLUE);
                }
            }
        });
    }

    private void setPulseMsg(double pulse) {
        int pulseInt = (int) pulse;
        final String pulseMsg = String.valueOf(pulseInt);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
textPulse.setText(pulseMsg);
            }
        });
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    @Override
    public Mat onCameraFrame(Mat inputFrame) {
        eventAggregator.triggerEvent(Event.NEW_IMAGE, inputFrame, null);
        return inputFrame;
    }

    /** Called when the user clicks the Send button */
    public void openConfig(View view) {
        Intent intent = new Intent(this, ConfigActivity.class);
        startActivity(intent);
    }

}
